# PythonBasico

# Sangría de Python
La sangría se refiere a los espacios al comienzo de una línea de código.

Mientras que en otros lenguajes de programación la sangría en el código es solo para legibilidad, la sangría en Python es muy importante.

Python usa sangría para indicar un bloque de código.

# Variables de Python
En Python, las variables se crean cuando le asigna un valor:


# Creando Variables
Las variables son contenedores para almacenar valores de datos.

A diferencia de otros lenguajes de programación, Python no tiene ningún comando para declarar una variable.

Una variable se crea en el momento en que le asigna un valor por primera vez.

# Tipos de datos
En programación, el tipo de datos es un concepto importante.

Las variables pueden almacenar datos de diferentes tipos y los diferentes tipos pueden hacer cosas diferentes.

Python tiene los siguientes tipos de datos integrados de forma predeterminada, en estas categorías:

Tipo de texto:	str
Tipos numéricos:	int, float, complex
Tipos de secuencia:	list, tuple, range
Tipo de mapeo:	dict
Tipos de conjuntos:	set, frozenset
Tipo booleano:	bool
Tipos binarios:	bytes, bytearray, memoryview

# Configuración del tipo de datos
En Python, el tipo de datos se establece cuando asigna un valor a una variable:
```python

x = "Hello World"	str	
x = 20	int	
x = 20.5	float	
x = 1j	complex	
x = ["apple", "banana", "cherry"]	list	
x = ("apple", "banana", "cherry")	tuple	
x = range(6)	range	
x = {"name" : "John", "age" : 36}	dict	
x = {"apple", "banana", "cherry"}	set	
x = frozenset({"apple", "banana", "cherry"})	frozenset	
x = True	bool	
x = b"Hello"	bytes	
x = bytearray(5)	bytearray	
x = memoryview(bytes(5))	memoryview
```

# Operadores aritméticos de Python
Los operadores aritméticos se utilizan con valores numéricos para realizar operaciones matemáticas comunes:

```python
Operator Name Example	
+	Addition	x + y	
-	Subtraction	x - y	
*	Multiplication	x * y	
/	Division	x / y	
%	Modulus	x % y	
**	Exponentiation	x ** y	
//	Floor division	x // y
```

# Operadores de asignación de Python
Los operadores de asignación se utilizan para asignar valores a las variables:


```python
Operator Example Same As	
=	x = 5	x = 5	
+=	x += 3	x = x + 3	
-=	x -= 3	x = x - 3	
*=	x *= 3	x = x * 3	
/=	x /= 3	x = x / 3	
%=	x %= 3	x = x % 3	
//=	x //= 3	x = x // 3	
**=	x **= 3	x = x ** 3	
&=	x &= 3	x = x & 3	
|=	x |= 3	x = x | 3	
^=	x ^= 3	x = x ^ 3	
>>=	x >>= 3	x = x >> 3	
<<=	x <<= 3	x = x << 3
```

# Operadores de comparación de Python
Los operadores de comparación se utilizan para comparar dos valores:


```python
Operator Name Example	
==	Equal	x == y	
!=	Not equal	x != y	
>	Greater than	x > y	
<	Less than	x < y	
>=	Greater than or equal to	x >= y	
<=	Less than or equal to	x <= y
```
# Operadores lógicos de Python
Los operadores lógicos se utilizan para combinar declaraciones condicionales:


```python

Operator Description Example	
and 	Returns True if both statements are true	x < 5 and  x < 10	
or	Returns True if one of the statements is true	x < 5 or x < 4	
not	Reverse the result, returns False if the result is true	not(x < 5 and x < 10)
```