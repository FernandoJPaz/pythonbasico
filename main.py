def main():
    print("Hola mundo")
    fib(15)
    myfunc()
    myfunc()

def fib(n):
    a, b = 0, 1
    while a < n:
        print(a, end=' ')
        a, b = b, a+b

    print()



MiVariable = "Martes"

# ESTE ES UN COMENTARIO DE UNA LINEA

''' 
ESTE ES UN COMENTARIO MULTILINEA
una linea
dos lineas 
tres lineas 
@@@@@
!!!!!
#####
%%%%%
'''

#python main.py

#Python usa sangría para indicar un bloque de código.
if 5 > 2:
  print("Five is greater than two!")

#Python le dará un error si omite la sangría:
if 5 > 2:
  print("Ejemplo correcto")

#El número de espacios depende de usted como programador, pero debe ser al menos uno.
if 5 > 2:
            print("Five is greater than two!") 
if 5 > 2:
    print("Five is greater than two!--------") 

#Tienes que usar la misma cantidad de espacios en el mismo bloque de código, de lo contrario Python te dará un error:
#if 5 > 2:
#  print("Five is greater than two!")
#      print("Five is greater than two!")

#IF ANIDADO 
print("----------")
if 10 > 5:
  if 5 > 1:
    print("5 es mayor a 1")
    if 100 > 99:
      print("IF Anidado")
  print("10 es mayor a 5")
print("----------")

print("Hola estoy fuera del if ")
x = 10
y = 0.5
z = x // y

print(z)


#Python no tiene ningún comando para declarar una variable.
x = 5
y = "Hello, World!"

x1 = 5
y2 = "John"
print(x1)
print(y2)

#Las variables no necesitan declararse con ningún tipo en particular e incluso pueden cambiar de tipo después de que se hayan establecido.
x = 4 # x is of type int
x = "Sally" # x is now of type str
print(x)

#Las variables de cadena se pueden declarar utilizando comillas simples o dobles:
x = "John"
# is the same as
x = 'John ---'

print(x)

'''
El nombre de una variable debe comenzar con una letra o el carácter de subrayado
Un nombre de variable no puede comenzar con un número
El nombre de una variable solo puede contener caracteres alfanuméricos y guiones bajos (Az, 0-9 y _)
Los nombres de las variables distinguen entre mayúsculas y minúsculas (la edad, la edad y la edad son tres variables diferentes)
'''
#Legal variable names:
myvar = "John"
MYVAR = "John F Kenedy"

my_var = "John"
_my_var = "John"
myVar = "John"
myvar2 = "John"

#Illegal variable names:
'''
2myvar = "John"
my-var = "John"
my var = "John"
'''

def myfunc():
  print("Python is " + MiVariable)

#Asignar valor a múltiples variables
#Python le permite asignar valores a múltiples variables en una línea:
x, y, z = "Orange", "Banana", "Cherry"
MiVariable = "No hoy es miercoles"
print(x)
print(y)
print(z)
#Y puede asignar el mismo valor a múltiples variables en una línea:
x = y = z = "Orange"
print(x)
print(y)
print(z)

#Variables de salida
#La print declaración de Python se usa a menudo para generar variables.

#Para combinar texto y una variable, Python usa el + carácter:
x = "awesome"
print("Python is " + x +" : "+ x)

#Para los números, el + carácter funciona como operador matemático:
#Para Concatenar una cadena con un numero deben de utilizar ','
x = 5
y = 10
z = x + y 
print("La suma es:", z )

'''
#Si intenta combinar una cadena y un número, Python le dará un error:
x = 5
y = "John"
print(x + y)
'''

#Variables globales
#Las variables que se crean fuera de una función (como en todos los ejemplos anteriores) se conocen como variables globales.
#Todo el mundo puede utilizar las variables globales, tanto dentro como fuera de las funciones

x = "awesome global"

if __name__ == "__main__":
   main()